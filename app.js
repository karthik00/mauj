var fs = require('fs');
var _=require('underscore')

var MongoClient = require('mongodb').MongoClient;

MongoClient.connect("mongodb://localhost:27017/mauj", function(err, db) {
  if(!err) {
  		db.collection('projects', function(err, collection) {
  			//creation of project
			fs.readFile(__dirname+'/simul/create_projext.json', 'utf8', function(err, data){
				var email_data=JSON.parse(data);
				var project={}
				var hash_patt=/#[a-zA-Z_]+/;
				var name_patt=/[A-Za-z]+$/;
				project.hashKey=hash_patt.exec(email_data.subjectline)[0];
				project.name=name_patt.exec(email_data.subjectline)[0];
				project.contributors=_.filter(email_data.to, function (item) {return item!="bot@man.com"}).concat(
											_.filter(email_data.cc, function (item) {return item!="bot@man.com"})
										);
				project.desc=email_data.emailbody;
				collection.insert(project, function (err, doc) {
					if (!err) {
						console.log("success");
					}
				});
			});
  		});

  }
});